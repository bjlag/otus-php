<?php

namespace Framework\Router\Exception;

class RouteNotMatchedException extends \Exception
{
}
