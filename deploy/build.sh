sudo cp -f ./deploy/nginx/app.conf /etc/nginx/sites-available
sudo ln -sf /etc/nginx/sites-available/app.conf /etc/nginx/sites-enabled
sudo service nginx restart
sudo service php7.3-fpm restart

sudo -u www-data cp .env.example .env
sudo -u www-data composer install -q
sudo -u www-data php ./bin/app.php migrations:migrate --no-interaction